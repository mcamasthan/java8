package com.java8.features;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stream<String> ster=Stream.of("abcttyty","bbcdfsdfdsf","ccdrt","gd","dsdf","dfsdf","dfdsf");
		//Optional<String> op=ster.max(Comparator.comparing(str->str.toString().length()));
		//Optional<String> op=	ster.reduce((a,b)->a.length()>b.length()?a:b);
		//System.out.println(op.get());
		
	//	ster.limit(2).forEach(System.out::println);
	//	ster.skip(3).forEach(System.out::println);
		List<String> list=Arrays.asList("masthan","ashok","raja","chitra","abc","raja");
		
		//list.stream().filter(s->s.equals("raja")).forEach(System.out::println);
		
		//list.stream().distinct().forEach(System.out::println);
		
	//	list.stream().map(s->s.toUpperCase()).forEach(System.out::println);
		
		List<Integer> intList=Arrays.asList(1,2,4,5,6,9,7);
		//intList.stream().map(i->i*2).forEach(System.out::println);
	//	Integer sum=intList.stream().mapToInt(i->i).sum();
	//	System.out.println(sum);
		
		IntSummaryStatistics res= intList.stream().mapToInt(i->i).summaryStatistics();
		//System.out.println(res.getAverage());
		
		List<Integer> list1=Arrays.asList(1,2,4,5,6,9,7);
		
		List<Integer> list2=Arrays.asList(10,11,23,56,80);
		
		String[][] array = new String[][]{{"a", "b"}, {"c", "d"}, {"e", "f"}};
		
		List<String[]> arrayList=Arrays.asList(array);
		
		//arrayList.stream().flatMap(Stream::of).forEach(System.out::println);       // Stream<String>
        
	//	IntStream.range(1, 10).forEach(System.out::println);
		
		//IntStream.rangeClosed(1, 10).forEach(System.out::println);
		
	//long count=	IntStream.rangeClosed(1, 10).filter(i->i%2==1).peek(System.out::println).count();
	
	//System.out.println(count);
	
	
	List<Employee> listEmp=new ArrayList<Employee>();
	listEmp.add(new Employee(4, "prasad", 3, 2200, "active"));
	listEmp.add(new Employee(5, "saradha", 4, 4000, "inactive"));
	listEmp.add(new Employee(6, "srinicas", 4, 5000, "active"));
	listEmp.add(new Employee(1, "masthan", 1, 200, "active"));
	listEmp.add(new Employee(2, "ashok", 1, 200, "active"));
	listEmp.add(new Employee(3, "raja", 2, 2500, "inactive"));

	
	//listEmp.stream().filter(s->s.getName().startsWith("s") && s.getSalary()>4000).collect(Collectors.toList()).forEach(e->{System.out.println(e.getName());});
	
	Map<Integer,Long> map=listEmp.stream().filter(emp->emp.getStatus().equals("active")).collect(Collectors.groupingBy(Employee::getDepId,Collectors.counting()));
	
	//map.forEach((k,v)->{System.out.println("dep :::::"+k+"  count ::::::"+v);});
	
	listEmp.stream().sorted(Comparator.comparing(Employee::getName)).forEach(emp->{System.out.println("the emp Names:"+emp.getName());});
		
		
		
		
		
		
		
		
		
		
	}

}
