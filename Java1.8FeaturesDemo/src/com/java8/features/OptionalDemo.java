package com.java8.features;

import java.util.Optional;

public class OptionalDemo {

	public static void main(String[] args) {
		
		String[] str = new String[10];// default object , String default value is null
		str[5]="Hello worlD";
		Optional<String> checkNull = Optional.ofNullable(str[5]);
		if (checkNull.isPresent()) {
			String word = checkNull.get();
			System.out.print(word);
		} else
			System.out.println("string is null");
	}
}