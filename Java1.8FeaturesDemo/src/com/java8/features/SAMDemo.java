package com.java8.features;

interface MyInterfaceDemo1{  
 
    default void newMethod(){  
        System.out.println(" MyInterfaceDemo1 Newly added default method");  
    }  
    void existingMethod(String str);  
}  
interface MyInterface2{  
	 
    default void newMethod(){  
        System.out.println("MyInterface2 Newly added default method");  
    }  
    void disp(String str);  
} 
public class SAMDemo implements MyInterfaceDemo1, MyInterface2{ // newMethod()
	// implementing abstract methods
    public void existingMethod(String str){           
        System.out.println("String is: "+str);  
    }  
    public void disp(String str){
    	System.out.println("String is: "+str); 
    }
    //Implementation of duplicate default method
    public void newMethod(){  
    	MyInterface2.super.newMethod();
        System.out.println("Implementation of default method");  
    }  
    public static void main(String[] args) {  
    	SAMDemo obj = new SAMDemo();
    	
    	//calling the default method of interface
       obj.newMethod();     
  
  
    }  
}