package com.java8.features;

public class Employee {
	
	private int empId;
	private String name;
	private int depId;
	private int salary;
	private String status;
	
	
	
	
	public Employee(int empId, String name, int depId, int salary, String status) {
		super();
		this.empId = empId;
		this.name = name;
		this.depId = depId;
		this.salary = salary;
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDepId() {
		return depId;
	}
	public void setDepId(int depId) {
		this.depId = depId;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	

}
