package com.java8.features;


public class Demo {  
    public void myMethod(){  
	System.out.println("Instance Method");  
    }  
    public static void main(String[] args) {  
	Demo obj = new Demo();   
	// Method reference using the object of the class
	MyInterfaceDemo ref = obj::myMethod;  
	// Calling the method of functional interface  
	ref.display();  
    }  
}

interface MyInterfaceDemo{  
    void display();  
}  