package com.java8.features;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class Java8Dates {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Instant 
		//Duration
		//Period
		//LocalDate
		//LocalTime
		//LocalDateTime
		//ZoneId
		//ZoneDateTime
		Date d=new Date();
		System.out.println(d);
		Instant start=Instant.now();
		System.out.println(start);
		for(int i=0;i<10000;i++){
			//System.out.println("tst"+i);
		}
		Instant end=Instant.now();
		
		Duration du=Duration.between(start, end);
		System.out.println(du.getSeconds());
		
		LocalTime lt=LocalTime.now();
		System.out.println( lt);
		LocalDate ld=LocalDate.now();
		System.out.println(ld);
		
		LocalDateTime ltd=LocalDateTime.now();
		System.out.println(ltd);
		
		ZoneId zd=ZoneId.systemDefault();
		
	//	ZoneId.getAvailableZoneIds().forEach(System.out::println);
		ZoneId test=ZoneId.of("Europe/Belgrade");
		ZonedDateTime zdt=ZonedDateTime.now(test);
		System.out.println(zdt);
		
		
	//	System.out.println(zd);
		
		
		
		
		

	}

}
