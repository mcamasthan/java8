package com.java8.features;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

 
public class ForeachDemo {  
     public static void main(String[] args) {  
        List<String> subList = new ArrayList<String>();  
        subList.add("Maths");  
        subList.add("English");  
        subList.add("French");  
        subList.add("Sanskrit");
        subList.add("Abacus");
        
        
        // before java 1.8
        
        Iterator  iterator=subList.iterator();
        
        while(iterator.hasNext()){
        	System.out.println(iterator.next());
        }
        System.out.println("------------before java1.8-------------");  
        for(String str:subList){
        	System.out.println(str);
        }
        
        
        System.out.println("-----------After java 1.8--------------");  
        
        subList.forEach(sub -> System.out.println(sub));  // Consumer
  }  
}