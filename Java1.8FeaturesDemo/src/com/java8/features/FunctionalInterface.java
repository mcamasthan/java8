package com.java8.features;

public class FunctionalInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyInterface sfi=(a,b)->System.out.println(a+b);
		sfi.test(20, 40);
		sfi.default_Fun();
		
	}

}


interface MyInterface 
{ 
     void test(int x,int y); 
 
     default void default_Fun() 
    { 
         System.out.println("This is default method"); 
    } 
     
     static void static_method(){
    	 System.out.println("This is static method"); 
     }
} 
