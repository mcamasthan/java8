package com.java8.features;

public class LamdaExpressionDemo {
	public static void main(String[] args) {
		
		/*Runnable t=()->System.out.print("Run!!1");
		new Thread(t).start();*/
		
		WithoutParams wop=()->System.out.println("this is test withoutparams");
		wop.test();
		
		WithParams wp=(a,b)-> a+b;
		int res=wp.testParams(10,40);
		
		System.out.println("the result is:"+res);
	}

	


}

interface WithoutParams{
	
	public void test();
	
}
interface WithParams{
	
	public int testParams(int a,int b);
	
}
